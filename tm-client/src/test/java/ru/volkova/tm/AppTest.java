package ru.volkova.tm;

import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import ru.volkova.tm.bootstrap.Bootstrap;

public class AppTest {

    @Rule
    public ExpectedSystemExit expectedSystemExit = ExpectedSystemExit.none();

    @Test
    public void showVersion() throws Exception {
        expectedSystemExit.expectSystemExitWithStatus(0);
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run("-v");
    }

    @Test
    public void showInfo() throws Exception {
        expectedSystemExit.expectSystemExitWithStatus(0);
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run("-i");
    }

}
