package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.endpoint.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "update project by index";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        @Nullable final Session session = bootstrap.getSession();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Project project = endpointLocator.getProjectEndpoint()
                .findProjectByIndex(session, index);
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().updateProjectByIndex(session, index, name, description);
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-by-index";
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
