package ru.volkova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.User;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

public class UserViewProfileCommand extends AbstractAuthCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "view user profile";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        System.out.println("[VIEW PROFILE]");
        final Session session = bootstrap.getSession();
        if (session == null) throw new ObjectNotFoundException();
        final String userId = bootstrap.getUserId(session);
        @NotNull final User user = endpointLocator.getAdminUserEndpoint().findUserById(session, userId);
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getSecondName());
        System.out.println("MIDDLE NAME" + user.getMiddleName());
    }

    @Override
    public String name() {
        return "user-view-profile";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
