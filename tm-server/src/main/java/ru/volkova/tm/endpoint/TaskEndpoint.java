package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.ITaskEndpoint;
import ru.volkova.tm.api.service.ITaskService;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.entity.Session;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint (@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @WebMethod
    public Task addTask(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "entity", partName = "entity") Task entity
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().add(entity).orElse(null);
    }

    @WebMethod
    public void removeTask(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "entity", partName = "entity") Task entity
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(entity);
    }

    @WebMethod
    public void addAllTasks(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "entities", partName = "entities") List<Task> entities)
    {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().addAll(entities);
    }

    @WebMethod
    public void clearTasks(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getTaskService().clear(userId);
    }

    @NotNull
    @WebMethod
    public List<Task> findAllTasks(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService().findAll(userId);
    }

    @Nullable
    @WebMethod
    public Task findTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService().findById(userId,id).orElse(null);
    }

    @WebMethod
    public void removeTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getTaskService().removeById(userId, id);
    }

    @Nullable
    @WebMethod
    public Task changeTaskStatusById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "id") Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService().changeOneStatusById(userId, id, status).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task changeTaskStatusByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService()
                .changeOneStatusByIndex(userId, index, status).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task changeTaskStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService()
                .changeOneStatusByName(userId, name, status).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task findTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService()
                .findOneByIndex(userId, index).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task findTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService().findOneByName(userId, name).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task finishTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService().finishOneById(userId, id).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task finishTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService().finishOneByIndex(userId, index).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task finishTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService().finishOneByName(userId, name).orElse(null);
    }

    @WebMethod
    public void removeTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getTaskService().removeOneByIndex(userId, index);
    }

    @WebMethod
    public void removeTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        serviceLocator.getTaskService().removeOneByName(userId, name);
    }

    @Nullable
    @WebMethod
    public Task startTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService().startOneById(userId, id).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task startTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService().startOneByIndex(userId, index).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task startTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService().startOneByName(userId, name).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task updateTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService()
                .updateOneById(userId, id, name, description).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task updateTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService()
                .updateOneByIndex(userId, index, name, description).orElse(null);
    }

    @Nullable
    @WebMethod
    public Task addTaskByUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getTaskService()
                .add(userId, name, description).orElse(null);
    }

}
