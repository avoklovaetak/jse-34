package ru.volkova.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.comparator.ComparatorByCreated;
import ru.volkova.tm.comparator.ComparatorByDateStart;
import ru.volkova.tm.comparator.ComparatorByName;
import ru.volkova.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("sort by name", ComparatorByName.getInstance()),
    CREATED("sort by created", ComparatorByCreated.getInstance()),
    DATE_START("sort by date start", ComparatorByDateStart.getInstance()),
    STATUS("sort by status", ComparatorByStatus.getInstance());

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.comparator = comparator;
        this.displayName = displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
