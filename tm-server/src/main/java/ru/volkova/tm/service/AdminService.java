package ru.volkova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.Data;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.IAdminService;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.constant.DataConst;
import ru.volkova.tm.dto.Domain;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class AdminService implements IAdminService {

    private ServiceLocator serviceLocator;

    public AdminService() {
    }

    public AdminService(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @SneakyThrows
    public void DataBinarySave() {
        System.out.println("[SAVE DATA BINARY]");
        @NotNull final Domain domain = new Domain();
        @NotNull final File file = new File(DataConst.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @SneakyThrows
    public void DataBinaryLoad() {
        System.out.println("[LOAD DATA BINARY]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConst.FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getBackupService().setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
        if (serviceLocator == null) return;
        serviceLocator.getAuthService().logout();
    }

    @SneakyThrows
    public void DataJsonLoad() {
        @NotNull final File file = new File(DataConst.FILE_JSON);
        if (!file.exists()) return;
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(DataConst.FILE_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        serviceLocator.getBackupService().setDomain(domain);
    }

    @SneakyThrows
    public void DataJsonSave() {
        @NotNull final Domain domain = serviceLocator.getBackupService().getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.FILE_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @SneakyThrows
    public void DataBase64Load() {
        @NotNull final String base64 = new String(Files.readAllBytes(Paths.get(DataConst.FILE_BASE64)));
        final byte[] decodeData = new BASE64Decoder().decodeBuffer(base64);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodeData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getBackupService().setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
        if (serviceLocator == null) return;
        serviceLocator.getAuthService().logout();
    }

    @SneakyThrows
    public void DataBase64Save() {
        System.out.println("[SAVE DATA BASE64]");
        @NotNull final Domain domain = serviceLocator.getBackupService().getDomain();

        @NotNull final File file =  new File(DataConst.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.FILE_BASE64);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @SneakyThrows
    @Override
    public void DataJsonLoadFasterxml() {
        System.out.println("[LOAD JSON DATA]");
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(DataConst.FILE_FASTERXML_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        serviceLocator.getBackupService().setDomain(domain);
    }

    @SneakyThrows
    @Override
    public void DataJsonSaveFasterxml() {
        System.out.println("[SAVE JSON DATA]");
        @NotNull final Domain domain = serviceLocator.getBackupService().getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.FILE_FASTERXML_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @SneakyThrows
    @Override
    public void DataJsonLoadJaxb() {
        System.out.println("[JSON DATA JAXB LOAD]");
        System.setProperty(DataConst.SYSTEM_JSON_PROPERTY_NAME, DataConst.SYSTEM_JSON_PROPERTY_VALUE);
        @NotNull final File file = new File(DataConst.FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(DataConst.JAXB_JSON_PROPERTY_NAME, DataConst.JAXB_JSON_PROPERTY_VALUE);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        serviceLocator.getBackupService().setDomain(domain);
    }

    @SneakyThrows
    @Override
    public void DataJsonSaveJaxb() {
        System.out.println("[SAVE JSON DATA TO JAXB]");
        System.setProperty(DataConst.SYSTEM_JSON_PROPERTY_NAME, DataConst.SYSTEM_JSON_PROPERTY_VALUE);
        @NotNull final Domain domain = serviceLocator.getBackupService().getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(DataConst.JAXB_JSON_PROPERTY_NAME, DataConst.JAXB_JSON_PROPERTY_VALUE);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.FILE_JAXB_JSON);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @SneakyThrows
    @Override
    public void DataXmlLoadFasterxml() {
        System.out.println("[LOAD XML DATA]");
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(DataConst.FILE_FASTERXML_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        serviceLocator.getBackupService().setDomain(domain);
    }

    @SneakyThrows
    @Override
    public void DataXmlSaveFasterxml() {
        System.out.println("[SAVE XML DATA]");
        @NotNull final Domain domain = serviceLocator.getBackupService().getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.FILE_FASTERXML_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @SneakyThrows
    @Override
    public void DataXmlLoadJaxb() {
        System.out.println("[XML DATA JAXB LOAD]");
        @NotNull final File file = new File(DataConst.FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        serviceLocator.getBackupService().setDomain(domain);
    }

    @SneakyThrows
    @Override
    public void DataXmlSaveJaxb() {
        System.out.println("[SAVE XML DATA TO JAXB]");
        @NotNull final Domain domain = serviceLocator.getBackupService().getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.FILE_JAXB_XML);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @SneakyThrows
    @Override
    public void DataYamlLoadFasterxml() {
        System.out.println("[LOAD YAML DATA]");
        @NotNull final String yaml = new String(Files.readAllBytes(Paths.get(DataConst.FILE_FASTERXML_YAML)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
        serviceLocator.getBackupService().setDomain(domain);
    }

    @SneakyThrows
    @Override
    public void DataYamlSaveFasterxml() {
        System.out.println("[SAVE YAML DATA]");
        @NotNull final Domain domain = serviceLocator.getBackupService().getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.FILE_FASTERXML_YAML);
        fileOutputStream.write(yaml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
